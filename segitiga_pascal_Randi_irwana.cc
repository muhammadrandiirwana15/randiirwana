#include <iostream>
#include <iomanip>
#include <stdlib.h>

using namespace std;

int faktorial(int n) {
    if (n == 0 || n == 1) {
        return 1;
    } else {
        return n * faktorial(n - 1);
    }
}

int kombinasi(int n, int k) {
    return faktorial(n) / (faktorial(k) * faktorial(n - k));
}

int main() {
    int n;

    
    cout << "Masukkan N : ";
    cin >> n;

    for (int i = 0; i < n; ++i) {
        for (int j = 0; j < n - i - 1; ++j) {
            cout << " ";
        }
        for (int k = 0; k <= i; ++k) {
            cout << kombinasi(i, k) << " ";
        }
        cout << endl;
    }
    return 0;
}    
