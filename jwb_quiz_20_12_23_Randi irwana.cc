#include <iostream>
#include <stdlib.h>

using namespace std;

void jdl_aplikasi();
void gapok_tunja(char gol, string status, float &gapok, float &tunja);
float prosen_potongan(float gapok);
float potongan(float gapok, float tunja, float prosen_pot);
float gaji_bersih(float gapok, float tunja, float pot);
void input(string &nama, char &gol, string &status);
void output(float gapok, float tunja, float pot, float gaber);

int main()
{
    system("clear");

    string nama = " ";
    char gol = ' ';
    string status = " ";

    float gapok = 0.0;
    float gaber = 0.0;
    float tunja = 0.0;
    float pot = 0.0;
    float prosen_pot = 0.05;

    jdl_aplikasi();

    input(nama, gol, status);

    gapok_tunja(gol, status, gapok, tunja);

    if (gapok > 300000)
    {
        prosen_pot = 0.1;
    }

    pot = potongan(gapok, tunja, prosen_pot);
    gaber = gaji_bersih(gapok, tunja, pot);

    output(gapok, tunja, pot, gaber);

    return 0;
}

void jdl_aplikasi()
{
    cout << "*************************" << endl;
    cout << "*  Aplikasi Hitung Gaji *" << endl;
    cout << "*************************" << endl
         << endl;
}

void gapok_tunja(char gol, string status, float &gapok, float &tunja)
{
    switch (gol)
    {
    case 'A':
        gapok = 200000;
        if (status == "Nikah")
        {
            tunja = 50000;
        }
        else
        {
            if (status == "Belum")
            {
                tunja = 25000;
            }
        }
        break;
    case 'B':
        gapok = 350000;
        if (status == "Nikah")
        {
            tunja = 75000;
        }
        else
        {
            if (status == "Belum")
            {
                tunja = 60000;
            }
        }
        break;
    default:
        cout << " Salah Input Golongan !!!" << endl;
        break;
    }
}

float prosen_potongan(float gapok)
{
    float prosen_pot = 0.0;

    return prosen_pot;
}

float potongan(float gapok, float tunja, float prosen_pot)
{
    float pot = (gapok + tunja) * prosen_pot;

    return pot;
}

float gaji_bersih(float gapok, float tunja, float pot)
{
    float gaber = (gapok + tunja) - pot;

    return gaber;
}

void input(string &nama, char &gol, string &status)
{
    cout << "Nama Karyawan       : ";
    cin >> nama;
    cout << "Golongan (A\\B)      : ";
    cin >> gol;
    cout << "Satus (Nikah\\Belum) : ";
    cin >> status;
}

void output(float gapok, float tunja, float pot, float gaber)
{
    cout << "Gaji Pokok     : Rp." << gapok << endl;
    cout << "Tunjangan      : Rp." << tunja << endl;
    cout << "Potongan Iuran : Rp." << pot << endl;
    cout << "Gaji Bersih    : Rp." << gaber << endl;
}
