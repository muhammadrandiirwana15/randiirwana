#include <iostream>
using namespace std;

double hitungNilai(double absen, double tugas, double quiz, double uts, double uas) {
    double nilai = ((0.1 * absen) + (0.2 * tugas) + (0.3 * quiz) + (0.4 * uts) + (0.5 * uas)) / 2;
    return nilai;
}

char tentukanHurufMutu(double nilai) {
    char hurufMutu;

    if (nilai > 85 && nilai <= 100)
        hurufMutu = 'A';
    else if (nilai > 70 && nilai <= 85)
        hurufMutu = 'B';
    else if (nilai > 55 && nilai <= 70)
        hurufMutu = 'C';
    else if (nilai > 40 && nilai <= 55)
        hurufMutu = 'D';
    else if (nilai >= 0 && nilai <= 40)
        hurufMutu = 'E';

    return hurufMutu;
}

int main() {
    double nilai, quiz, absen, uts, uas, tugas;
    char Huruf_Mutu;

    quiz = 40; absen = 100; uts = 60; uas = 50; tugas = 80;

    cout << "Absen = " << absen << " UTS = " << uts << endl;
    cout << "Tugas = " << tugas << " UAS = " << uas << endl;
    cout << "Quiz = " << quiz << endl;
    
    nilai = hitungNilai(absen, tugas, quiz, uts, uas);

    Huruf_Mutu = tentukanHurufMutu(nilai);

    cout << "Huruf Mutu : " << Huruf_Mutu << endl;

    return 0;
}
